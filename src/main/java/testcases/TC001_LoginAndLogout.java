package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginAndLogout";
		testDescription = "Login into LeafTaps";
		testNodes = "Leads";
		authors = "Swami";
		category = "Smoke";
		dataSheetName = "TC001";
	}	

	@Test(dataProvider = "fetchData")
	public void loginAndLogout(String uname, String pwd, String verifyText) {
		/*LoginPage lp = new LoginPage();
		lp.enterUserName(uname);
		lp.enterPassword(pwd);*/
		new LoginPage()
		.enterUserName(uname)
		.enterPassword(pwd)
		.clickLogin()
		.verifyHomePageText(verifyText)
		.clickLogout();
	}
}
