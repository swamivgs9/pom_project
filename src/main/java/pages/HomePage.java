package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH, using = "//h2[text()='Welcome ']") WebElement eleText;
	public HomePage verifyHomePageText(String data) {
		verifyPartialText(eleText, data);
		return this;
	}
	
	@FindBy(how=How.XPATH, using = "//a[contains(text(),'CRM/SFA')]") WebElement eleCrmsfa;
	public LeadsPage clickCrmsfa() {
		click(eleCrmsfa);
		return new LeadsPage();
	}
	
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		click(eleLogout);
		return new LoginPage(); 
	}
}























