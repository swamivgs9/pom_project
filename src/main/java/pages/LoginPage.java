package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID, using="username") WebElement eleUsername;
	public LoginPage enterUserName(String data) {
	    //WebElement eleUsername = locateElement("id", "LoginPage.id.username");
		type(eleUsername, data);
		return this;
	}
	
	@FindBy(how = How.ID, using="password") WebElement elePassword;
	public LoginPage enterPassword(String data) {
		// WebElement elePassword = locateElement("id", "LoginPage.id.password");
		type(elePassword, data);
		return this;
	}
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogin;
	public HomePage clickLogin() {
		click(eleLogin);
		/*HomePage hp = new HomePage();
		return hp; */
		return new HomePage();
	}
}























