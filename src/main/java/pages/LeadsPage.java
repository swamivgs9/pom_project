package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LeadsPage extends ProjectMethods{

	public LeadsPage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.XPATH, using = "//a[text()='Create Lead']") WebElement eleCreate;
	public CreateLeadPage clickCreateLead() {
		click(eleCreate);
		return new CreateLeadPage(); 
	}
}
