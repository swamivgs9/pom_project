package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH, using="//a[text()='Create Lead']") WebElement eleCreateLead;
	public CreateLeadPage createLead() {
		click(eleCreateLead);
		return this;
	}
	
	@FindBy(how = How.ID, using="//a[text()='Create Lead']") WebElement eleCmpnyName;
	public CreateLeadPage enterCompanyName(String data) {
		type(eleCmpnyName, data);
		return this;
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFrstName;
		public CreateLeadPage enterFirstName(String data) {
		type(eleFrstName, data);
			return this;
	}
	
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	public CreateLeadPage enterLastName(String data) {
		type(eleLastName, data);
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//input[@type='submit']") WebElement eleClick;
	public CreateLeadPage clickSubmit() {
		click(eleClick);
		return this;
	}
	
	
	
	
	
	
	
	
	
	
}
